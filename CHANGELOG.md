# Change Log
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [devel]
### Added
### Changed
### Fixed


## [0.0.10] - 22-July-2014
### Added
- CHANGELOG.md
- copied unmodified from https://git.pengutronix.de/cgit/tools/libsocketcan/

### Changed
- see https://git.pengutronix.de/cgit/tools/libsocketcan/ for logs

### Fixed
### Removed


